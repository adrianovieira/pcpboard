require 'spec_helper'
describe 'pcpboard' do
  let (:facts) {{
    :osfamily => 'Redhat'
  }}

  context 'with default values for all parameters' do
    it { is_expected.to compile }

    it { is_expected.to contain_class('pcpboard') }
  end
end
